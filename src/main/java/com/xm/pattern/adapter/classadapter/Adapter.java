package com.xm.pattern.adapter.classadapter;

public class Adapter extends Adaptee implements Target {
    @Override
    public void exec() {
        super.test();
    }
}
