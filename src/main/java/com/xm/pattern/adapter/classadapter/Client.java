package com.xm.pattern.adapter.classadapter;

public class Client {
    public static void main(String[] args) {
        Target target = new ConcreteTarget();
        target.exec();
        Target target1 = new Adapter();
        target1.exec();
    }
}
