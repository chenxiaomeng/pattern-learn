package com.xm.pattern.adapter.classadapter;

public class ConcreteTarget implements Target {
    @Override
    public void exec() {
        System.out.println(this.getClass().getName());
    }
}
