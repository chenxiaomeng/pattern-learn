package com.xm.pattern.adapter.classadapter;

public interface Target {
    public void exec();
}
