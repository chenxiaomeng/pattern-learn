package com.xm.pattern.adapter.objectadapter;

public interface Adaptee {
    public void test();
}
