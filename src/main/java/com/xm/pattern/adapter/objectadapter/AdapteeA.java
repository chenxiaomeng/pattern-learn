package com.xm.pattern.adapter.objectadapter;

public class AdapteeA implements Adaptee {
    @Override
    public void test() {
        System.out.println(this.getClass().getName());
    }
}
