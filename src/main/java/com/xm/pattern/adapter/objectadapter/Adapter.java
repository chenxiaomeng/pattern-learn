package com.xm.pattern.adapter.objectadapter;

public class Adapter implements Target {
    private Adaptee adaptee;

    public Adapter(Adaptee _adaptee) {
        this.adaptee = _adaptee;
    }

    @Override
    public void exec() {
        this.adaptee.test();
    }
}
