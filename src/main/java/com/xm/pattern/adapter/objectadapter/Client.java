package com.xm.pattern.adapter.objectadapter;

public class Client {
    public static void main(String[] args) {
        Target target = new ConcreteTarget();
        target.exec();
        Target target1 = new Adapter(new AdapteeA());
        target1.exec();
    }
}
