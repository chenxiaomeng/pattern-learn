package com.xm.pattern.adapter.objectadapter;

public class ConcreteTarget implements Target {
    @Override
    public void exec() {
        System.out.println(this.getClass().getName());
    }
}
