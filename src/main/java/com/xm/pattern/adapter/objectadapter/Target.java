package com.xm.pattern.adapter.objectadapter;

public interface Target {
    public void exec();
}
