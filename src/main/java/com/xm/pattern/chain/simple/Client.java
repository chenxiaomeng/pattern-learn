package com.xm.pattern.chain.simple;

public class Client {

    public static void test1(HandlerFac handlerFac) {
        Response responseM = handlerFac.getFirstHandler().handleMsg(new RequestA());
        System.out.println(responseM.getName());
    }

    public static void test2(HandlerFac handlerFac) {
        Response responseL = handlerFac.getFirstHandler().handleMsg(new RequestB());
        System.out.println(responseL.getName());
    }

    public static void main(String[] args) {
        HandlerFac handlerFac = new HandlerFac();
        test1(handlerFac);
        test2(handlerFac);
    }
}
