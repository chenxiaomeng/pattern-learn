package com.xm.pattern.chain.simple;

public abstract class Handler {

    public enum Level {
        High(1), Mid(2), Low(3);
        private Integer index;

        Level(int idx) {
            this.index = idx;
        }

        public Integer getIndex() {
            return this.index;
        }
    }

    private Handler nextHandler;

    public final Response handleMsg(Request request) {
        Response response = null;
        if (this.getHandleLvl().equals(request.getRequestLvl())) {
            response = run(request);
        } else {
            if (this.nextHandler != null) {
                response = this.nextHandler.handleMsg(request);
            } else {
                throw new RuntimeException("no handler!");
            }
        }
        return response;
    }

    public void setNext(Handler _handle) {
        this.nextHandler = _handle;
    }

    protected abstract Level getHandleLvl();

    protected abstract Response run(Request request);
}
