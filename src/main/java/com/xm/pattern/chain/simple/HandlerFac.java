package com.xm.pattern.chain.simple;

public class HandlerFac {
    private HandlerH handlerH;
    private HandlerM handlerM;

    public HandlerFac() {
        this.handlerH = new HandlerH();
        this.handlerM = new HandlerM();
        this.handlerH.setNext(handlerM);
    }

    public Handler getFirstHandler() {
        return handlerH;
    }
}
