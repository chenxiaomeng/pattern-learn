package com.xm.pattern.chain.simple;

public class HandlerH extends Handler {
    @Override
    protected Level getHandleLvl() {
        return Level.High;
    }

    @Override
    protected Response run(Request request) {
        return new Response(request.getReqName());
    }
}
