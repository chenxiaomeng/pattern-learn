package com.xm.pattern.chain.simple;

public class HandlerM extends Handler {
    @Override
    protected Level getHandleLvl() {
        return Level.Mid;
    }

    @Override
    protected Response run(Request request) {
        return new Response(request.getReqName());
    }
}
