package com.xm.pattern.chain.simple;

public interface Request {
    public Handler.Level getRequestLvl();
    public String getReqName();
}
