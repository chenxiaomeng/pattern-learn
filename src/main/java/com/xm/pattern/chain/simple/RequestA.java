package com.xm.pattern.chain.simple;

public class RequestA implements Request {
    @Override
    public Handler.Level getRequestLvl() {
        return Handler.Level.Mid;
    }

    @Override
    public String getReqName() {
        return this.getClass().getName();
    }
}
