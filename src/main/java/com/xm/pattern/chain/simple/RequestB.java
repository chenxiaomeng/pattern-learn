package com.xm.pattern.chain.simple;

public class RequestB implements Request {
    @Override
    public Handler.Level getRequestLvl() {
        return Handler.Level.Low;
    }

    @Override
    public String getReqName() {
        return this.getClass().getName();
    }
}
