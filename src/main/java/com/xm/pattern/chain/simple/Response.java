package com.xm.pattern.chain.simple;

public class Response {
    private String name;

    public Response(String _name) {
        this.name = _name;
    }

    public String getName() {
        return name;
    }
}
