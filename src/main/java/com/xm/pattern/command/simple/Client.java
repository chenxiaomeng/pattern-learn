package com.xm.pattern.command.simple;

public class Client {
    public static void main(String[] args) {
        Invoker invoker1 = new Invoker();
        invoker1.setCommand(new CommandA());
        invoker1.action();
        invoker1.setCommand(new CommandB());
        invoker1.action();
    }
}
