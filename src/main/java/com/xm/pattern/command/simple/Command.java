package com.xm.pattern.command.simple;

public abstract class Command {
    protected ReceiveC receiveC;

    public Command(ReceiveC _receiveC) {
        this.receiveC = _receiveC;
    }

    public abstract void execute();
}
