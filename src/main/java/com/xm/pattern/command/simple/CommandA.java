package com.xm.pattern.command.simple;

public class CommandA extends Command {
    private ReceiveA receiveA;

    public CommandA() {
        super(new ReceiveC());
        this.receiveA = new ReceiveA();
    }

    @Override
    public void execute() {
        super.receiveC.doC();
        this.receiveA.doA();
    }
}
