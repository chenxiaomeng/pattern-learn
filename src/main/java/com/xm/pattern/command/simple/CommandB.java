package com.xm.pattern.command.simple;

public class CommandB extends Command {
    private ReceiveB receiveB;

    public CommandB() {
        super(new ReceiveC());
        this.receiveB = new ReceiveB();
    }

    @Override
    public void execute() {
        super.receiveC.doC();
        this.receiveB.doB();
    }
}
