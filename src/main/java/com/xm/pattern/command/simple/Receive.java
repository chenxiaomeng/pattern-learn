package com.xm.pattern.command.simple;

public abstract class Receive {
    public abstract void doA();

    public abstract void doB();

    public abstract void doC();
}
