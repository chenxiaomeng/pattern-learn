package com.xm.pattern.command.simple;

public class ReceiveC extends Receive {

    @Override
    public void doA() {
        System.out.println(this.getClass().getName() + ":doA");
    }

    @Override
    public void doB() {
        System.out.println(this.getClass().getName() + ":doB");
    }

    @Override
    public void doC() {
        System.out.println(this.getClass().getName() + ":doC");
    }
}
