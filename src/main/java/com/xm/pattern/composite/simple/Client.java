package com.xm.pattern.composite.simple;

public class Client {
    public static void main(String[] args) {
        Component root = createTreeStruture();
        showAll(root);
    }

    public static Component createTreeStruture() {
        Composite root = new Composite("root");
        Composite branch1 = new Composite("branch1");
        branch1.setParent(root);
        Composite branch2 = new Composite("branch2");
        branch2.setParent(root);
        root.add(branch1);
        root.add(branch2);
        //------------------------------
        Composite branch11 = new Composite("branch11");
        branch11.setParent(branch1);
        branch1.add(branch11);
        Leaf leaf1 = new Leaf("leaf1");
        branch1.add(leaf1);
        //------------------------------
        return root;
    }

    public static void showAll(Component root) {
        root.getName();
        if (root instanceof Composite) {
            for (Component child : ((Composite) root).getChildren()) {
                showAll(child);
            }
        }
    }
}
