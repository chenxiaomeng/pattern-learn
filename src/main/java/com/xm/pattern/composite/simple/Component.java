package com.xm.pattern.composite.simple;

public abstract class Component {
    private String name;
    private Component parent;

    public Component(String name) {
        this.name = name;
    }

    public void getName() {
        System.out.println(this.name);
    }

    public Component getParent() {
        return parent;
    }

    public void setParent(Component parent) {
        this.parent = parent;
    }
}
