package com.xm.pattern.composite.simple;

import java.util.ArrayList;
import java.util.List;

public class Composite extends Component {
    private ArrayList<Component> children = new ArrayList<Component>();

    public void add(Component component) {
        this.children.add(component);
    }

    public void remove(Component component) {
        this.children.remove(component);
    }

    public List<Component> getChildren() {
        return this.children;
    }

    public Composite(String name) {
        super(name);
    }

    @Override
    public void getName() {
        System.out.println(this.getClass().getName());
        super.getName();
    }
}
