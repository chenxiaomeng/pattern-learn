package com.xm.pattern.composite.simple;

public class Leaf extends Component {

    public Leaf(String name) {
        super(name);
    }

    @Override
    public void getName() {
        System.out.println(this.getClass().getName());
        super.getName();
    }
}
