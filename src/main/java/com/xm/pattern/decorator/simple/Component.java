package com.xm.pattern.decorator.simple;

public abstract class Component {
    public abstract void operator();
}
