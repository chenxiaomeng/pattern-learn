package com.xm.pattern.decorator.simple;

public class ConcreteComponent extends Component {
    @Override
    public void operator() {
        System.out.println(this.getClass().getName());
    }
}
