package com.xm.pattern.decorator.simple;

public class ConcreteDecoratorA extends Decorator {
    public ConcreteDecoratorA(Component _component) {
        super(_component);
    }

    public void test1() {
        System.out.println(this.getClass().getName());
    }

    @Override
    public void operator() {
        this.test1();
        super.operator();
    }
}
