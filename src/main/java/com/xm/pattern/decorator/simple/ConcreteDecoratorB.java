package com.xm.pattern.decorator.simple;

public class ConcreteDecoratorB extends Decorator {
    public ConcreteDecoratorB(Component _component) {
        super(_component);
    }

    public void test2() {
        System.out.println(this.getClass().getName());
    }

    @Override
    public void operator() {
        super.operator();
        this.test2();
    }
}
