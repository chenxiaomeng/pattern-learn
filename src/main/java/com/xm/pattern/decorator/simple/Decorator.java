package com.xm.pattern.decorator.simple;

public abstract class Decorator extends Component {
    private Component component;

    public Decorator(Component _component) {
        this.component = _component;
    }

    @Override
    public void operator() {
        this.component.operator();
    }
}
