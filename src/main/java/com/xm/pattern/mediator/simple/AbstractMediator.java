package com.xm.pattern.mediator.simple;

public abstract class AbstractMediator {
    protected ConcreteColleagueA concreteColleagueA;
    protected ConcreteColleagueB concreteColleagueB;

    public AbstractMediator() {
        this.concreteColleagueA = new ConcreteColleagueA(this);
        this.concreteColleagueB = new ConcreteColleagueB(this);
    }

    public ConcreteColleagueA getConcreteColleagueA() {
        return concreteColleagueA;
    }

    public void setConcreteColleagueA(ConcreteColleagueA concreteColleagueA) {
        this.concreteColleagueA = concreteColleagueA;
    }

    public ConcreteColleagueB getConcreteColleagueB() {
        return concreteColleagueB;
    }

    public void setConcreteColleagueB(ConcreteColleagueB concreteColleagueB) {
        this.concreteColleagueB = concreteColleagueB;
    }

    public abstract void execute(String pattern);
}
