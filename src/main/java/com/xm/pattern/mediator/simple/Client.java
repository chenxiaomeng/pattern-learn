package com.xm.pattern.mediator.simple;

public class Client {
    public static void main(String[] args) {
        Mediator mediator = new Mediator();
        mediator.execute("ab");
        System.out.println("----------------");
        mediator.execute("ba");
    }
}
