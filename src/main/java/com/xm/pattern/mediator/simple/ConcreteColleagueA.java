package com.xm.pattern.mediator.simple;

public class ConcreteColleagueA extends AbstractColleague {

    public ConcreteColleagueA(AbstractMediator _mediator) {
        super(_mediator);
    }

    public void aSay(){
        this.mediator.getConcreteColleagueB().bSay();
        System.out.println(this.getClass().getName());
    }
}
