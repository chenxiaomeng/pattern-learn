package com.xm.pattern.mediator.simple;

public class ConcreteColleagueB extends AbstractColleague {

    public ConcreteColleagueB(AbstractMediator _mediator) {
        super(_mediator);
    }

    public void bSay() {
        this.mediator.getConcreteColleagueA().aSay();
        System.out.println(this.getClass().getName());
    }
}
