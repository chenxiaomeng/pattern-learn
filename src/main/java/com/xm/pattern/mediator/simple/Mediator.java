package com.xm.pattern.mediator.simple;

public class Mediator extends AbstractMediator {

    @Override
    public void execute(String pattern) {
        if (pattern.equalsIgnoreCase("ab")) {
            this.concreteColleagueA.aSay();
            this.concreteColleagueB.bSay();
        } else {
            this.concreteColleagueB.bSay();
            this.concreteColleagueA.aSay();
        }
    }
}
