package com.xm.pattern.memento.clone;

public class Originator implements Cloneable {
    private Originator mementor;

    private String status = "";

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void createMementor() {
        this.mementor = this.clone();
    }

    public void restoreMementor() {
        this.setStatus(this.mementor.status);
    }

    @Override
    protected Originator clone() {
        try {
            return (Originator) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
