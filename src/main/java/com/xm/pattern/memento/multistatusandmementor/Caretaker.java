package com.xm.pattern.memento.multistatusandmementor;

import java.util.HashMap;

public class Caretaker {
    private HashMap<String, Mementor> mementorHashMap = new HashMap<String, Mementor>();

    public Mementor getMementor(String key) {
        return this.mementorHashMap.get(key);
    }

    public void setMementor(String key, Mementor mementor) {
        this.mementorHashMap.put(key, mementor);
    }
}
