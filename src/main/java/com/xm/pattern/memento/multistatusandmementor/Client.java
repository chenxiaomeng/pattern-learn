package com.xm.pattern.memento.multistatusandmementor;

public class Client {
    public static void main(String[] args) {
        Originator originator = new Originator();
        Caretaker caretaker = new Caretaker();
        caretaker.setMementor("001", originator.createMementor());
        originator.restoreMementor(caretaker.getMementor("001"));
    }
}
