package com.xm.pattern.memento.multistatusandmementor;

import java.util.HashMap;

public class Mementor {
    private HashMap<String, Object> stateMap;

    public HashMap<String, Object> getStateMap() {
        return stateMap;
    }

    public void setStateMap(HashMap<String, Object> stateMap) {
        this.stateMap = stateMap;
    }

    public Mementor(HashMap<String, Object> map) {
        this.stateMap = map;
    }
}
