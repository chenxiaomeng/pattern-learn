package com.xm.pattern.memento.multistatusandmementor;

public class Originator {
    private String state = "";
    private String state1 = "";

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState1() {
        return state1;
    }

    public void setState1(String state1) {
        this.state1 = state1;
    }

    public Mementor createMementor() {
        return new Mementor(BeanUtils.backupProp(this));
    }

    public void restoreMementor(Mementor _mementor) {
        BeanUtils.restoreProp(this, _mementor.getStateMap());
    }
}
