package com.xm.pattern.memento.simple;

public class Caretaker {
    private Mementor mementor;

    public Mementor getMementor() {
        return mementor;
    }

    public void setMementor(Mementor mementor) {
        this.mementor = mementor;
    }
}
