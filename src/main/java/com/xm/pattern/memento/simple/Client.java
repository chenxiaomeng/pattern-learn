package com.xm.pattern.memento.simple;

public class Client {
    public static void main(String[] args) {
        Originator originator = new Originator();
        Caretaker caretaker = new Caretaker();
        caretaker.setMementor(originator.createMementor());
        originator.restoreMementor(caretaker.getMementor());
    }
}
