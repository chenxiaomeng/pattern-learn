package com.xm.pattern.memento.simple;

public class Mementor {
    private String state = "";

    public Mementor(String _status) {
        this.state = _status;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
