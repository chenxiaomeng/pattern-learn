package com.xm.pattern.memento.simple;

public class Originator {
    private String state = "";

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Mementor createMementor() {
        return new Mementor(this.state);
    }

    public void restoreMementor(Mementor _mementor) {
        this.setState(_mementor.getState());
    }
}
