package com.xm.pattern.observer.simple;

public class Client {
    public static void main(String[] args) {
        Subject subject = new Subject();
        Theme theme = new Theme();
        subject.addObserver(theme);
        subject.doSomething();
        subject.doSomething();
    }
}
