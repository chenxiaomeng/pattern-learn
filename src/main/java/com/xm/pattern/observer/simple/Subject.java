package com.xm.pattern.observer.simple;

import java.util.Observable;

public class Subject extends Observable {
    public void doSomething() {
        System.out.println("hello");
        super.setChanged();
        super.notifyObservers("say hello");
    }
}
