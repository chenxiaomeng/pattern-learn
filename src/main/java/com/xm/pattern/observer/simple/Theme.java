package com.xm.pattern.observer.simple;

import java.util.Observable;
import java.util.Observer;

public class Theme implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        System.out.println(arg.toString());
    }
}
