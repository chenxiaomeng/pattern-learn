package com.xm.pattern.prototype.simple;

import java.util.ArrayList;
import java.util.Arrays;

public class Client {

    public static void main(String[] args) {
        Template template = new Template('a', "a", new String[]{"a"}, new ArrayList(Arrays.asList("a")));
        MasterBusiness masterBusiness = new MasterBusiness(template);
        MasterBusiness masterBusiness1 = masterBusiness.clone();
        System.out.println(masterBusiness1.toString());

        MasterBusiness masterBusiness2 = masterBusiness.clone();
        System.out.println(masterBusiness2.toString());
        masterBusiness2.setMb_char('b');
        masterBusiness2.setMb_str("b");
        masterBusiness2.setMb_str_arr(new String[]{"b"});
        masterBusiness2.setMb_str_list(new ArrayList(Arrays.asList("b")));
        System.out.println(masterBusiness2.toString());
    }
}
