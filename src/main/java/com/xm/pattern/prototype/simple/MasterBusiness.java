package com.xm.pattern.prototype.simple;

import java.util.Arrays;
import java.util.List;

public class MasterBusiness implements Cloneable {
    private char mb_char;
    private String mb_str;
    private String[] mb_str_arr;
    private List<String> mb_str_list;

    public MasterBusiness(Template template) {
        this.mb_char = template.getMb_char();
        this.mb_str = template.getMb_str();
        this.mb_str_arr = template.getMb_str_arr();
        this.mb_str_list = template.getMb_str_list();
    }

    public char getMb_char() {
        return mb_char;
    }

    public void setMb_char(char mb_char) {
        this.mb_char = mb_char;
    }

    public String getMb_str() {
        return mb_str;
    }

    public void setMb_str(String mb_str) {
        this.mb_str = mb_str;
    }

    public String[] getMb_str_arr() {
        return mb_str_arr;
    }

    public void setMb_str_arr(String[] mb_str_arr) {
        this.mb_str_arr = mb_str_arr;
    }

    public List<String> getMb_str_list() {
        return mb_str_list;
    }

    public void setMb_str_list(List<String> mb_str_list) {
        this.mb_str_list = mb_str_list;
    }

    @Override
    public String toString() {
        return "MasterBusiness{" +
                "mb_char=" + mb_char +
                ", mb_str='" + mb_str + '\'' +
                ", mb_str_arr=" + Arrays.toString(mb_str_arr) +
                ", mb_str_list=" + mb_str_list +
                '}';
    }

    @Override
    public MasterBusiness clone() {
        MasterBusiness masterBusiness = null;
        try {
            masterBusiness = (MasterBusiness) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return masterBusiness;
    }
}
