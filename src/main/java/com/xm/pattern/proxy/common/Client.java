package com.xm.pattern.proxy.common;

public class Client {
    public static void main(String[] args) {
        Subject proxy = new Proxy("test");
        proxy.request();
    }
}
