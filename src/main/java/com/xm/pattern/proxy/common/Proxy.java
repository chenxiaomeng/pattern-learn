package com.xm.pattern.proxy.common;

public class Proxy implements Subject {
    private Subject subject;

    public Proxy(String _name) {
        this.subject = new RealSubject(this, _name);
    }

    private void before() {
        System.out.println("before");
    }

    private void after() {
        System.out.println("after");
    }

    @Override
    public void request() {
        before();
        subject.request();
        after();
    }
}
