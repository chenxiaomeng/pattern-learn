package com.xm.pattern.proxy.common;

public class RealSubject implements Subject {
    private String name;

    public RealSubject(Subject _subject, String _name) {
        if (_subject != null) {
            this.name = _name;
        }
    }

    @Override
    public void request() {
        System.out.println(this.name);
    }
}
