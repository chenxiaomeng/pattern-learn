package com.xm.pattern.proxy.common;

public interface Subject {
    public void request();
}
