package com.xm.pattern.proxy.dynamic;

public class Client {
    public static void main(String[] args) {
        Subject subject = new RealSubject("test");
        Subject subjectProxy = Proxy.newInstance(subject);
        subjectProxy.request();
    }
}
