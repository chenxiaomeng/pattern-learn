package com.xm.pattern.proxy.dynamic;

public class RealSubject implements Subject {
    private String name;

    public RealSubject(String _name) {
        this.name = _name;
    }

    @Override
    public void request() {
        System.out.println(this.name);
    }
}
