package com.xm.pattern.proxy.dynamic;

public interface Subject {
    public void request();
}
