package com.xm.pattern.proxy.force;

public class Client {
    public static void main(String[] args) {
        Subject realSubject = new RealSubject("test");
        realSubject.request();
        realSubject.getProxy().request();
    }
}
