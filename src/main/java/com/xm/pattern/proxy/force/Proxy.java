package com.xm.pattern.proxy.force;

public class Proxy implements Subject {
    private Subject subject;

    public Proxy(Subject _subject) {
        this.subject = _subject;
    }

    private void before() {
        System.out.println("before");
    }

    private void after() {
        System.out.println("after");
    }

    @Override
    public void request() {
        before();
        subject.request();
        after();
    }

    @Override
    public Subject getProxy() {
        return this;
    }
}
