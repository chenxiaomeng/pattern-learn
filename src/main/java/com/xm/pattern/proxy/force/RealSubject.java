package com.xm.pattern.proxy.force;

public class RealSubject implements Subject {
    private String name;
    private Subject proxy;

    public RealSubject(String _name) {
        this.name = _name;
    }

    @Override
    public void request() {
        if (this.isProxy()) {
            System.out.println(this.name);
        } else {
            System.out.println("no proxy");
        }
    }

    @Override
    public Subject getProxy() {
        synchronized (this
                ) {
            if (this.proxy == null) {
                this.proxy = new Proxy(this);
            }
        }
        return this.proxy;
    }

    private boolean isProxy() {
        if (this.proxy == null) {
            return false;
        } else {
            return true;
        }
    }
}
