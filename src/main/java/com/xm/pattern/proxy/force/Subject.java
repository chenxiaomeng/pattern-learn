package com.xm.pattern.proxy.force;

public interface Subject {
    public void request();

    public Subject getProxy();
}
