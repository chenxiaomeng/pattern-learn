package com.xm.pattern.strategy.enumpattern;

public class Client {
    public static void main(String[] args) {
        EnumStrategy enumStrategy = EnumStrategy.STRATEGY_A;
        enumStrategy.exec();
        enumStrategy = EnumStrategy.STRATEGY_B;
        enumStrategy.exec();
    }
}
