package com.xm.pattern.strategy.enumpattern;

public enum EnumStrategy {
    STRATEGY_A("a") {
        @Override
        public void exec() {
            System.out.println(getValue());
        }
    }, STRATEGY_B("b") {
        @Override
        public void exec() {
            System.out.println(getValue());
        }
    };


    private String value;

    private EnumStrategy(String sym) {
        this.value = sym;
    }

    public String getValue() {
        return this.value;
    }

    public abstract void exec();
}
