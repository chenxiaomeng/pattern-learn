package com.xm.pattern.strategy.simple;

public class Client {
    public static void main(String[] args) {
        Context context = new Context(new StrategyA());
        context.exec();
        context = new Context(new StrategyB());
        context.exec();
    }
}
