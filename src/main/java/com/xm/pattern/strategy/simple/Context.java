package com.xm.pattern.strategy.simple;

public class Context {
    private Strategy strategy;

    public Context(Strategy _strategy) {
        this.strategy = _strategy;
    }

    public void exec() {
        this.strategy.exec();
    }
}
