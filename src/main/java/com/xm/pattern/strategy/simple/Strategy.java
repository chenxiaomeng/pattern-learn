package com.xm.pattern.strategy.simple;

public interface Strategy {
    public void exec();
}
