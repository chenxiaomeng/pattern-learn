package com.xm.pattern.strategy.simple;

public class StrategyA implements Strategy {
    @Override
    public void exec() {
        System.out.println(this.getClass().getName());
    }
}
